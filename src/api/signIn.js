import axios from "axios";
import Swal from "sweetalert2";

export const handleSignIn = async (values, { setSubmitting }) => {
  console.log("🚀 ~ file: login.js ~ line 2 ~ handleSubmit ~ values", values);
  const config = {
    headers: {
      "Content-Type": "application/json",
    },
  };

  const body = {
    email: values.email,
    password: values.password,
  };
  try {
    const res = await axios.post(
      `https://my-clinic-apii.herokuapp.com/login`,
      body,
      config
    );
    console.log("🚀 ~ file: login.js ~ line 11 ~ handleSubmit ~ res", res.data);

    setSubmitting(false);

    localStorage.setItem("account", JSON.stringify(res.data));

    Swal.fire({
      position: "top-end",
      icon: "success",
      title: "Sign In successful ❤️",
      showConfirmButton: false,
      timer: 1500,
    });

    setTimeout(() => {
      window.location.href = `/healthReport/${res.data.user.id}`;
    }, 1700);
  } catch (error) {
    if (error.response) {
      Swal.fire({
        position: "top-end",
        icon: "error",
        title: "Can not Sign In 🤧",
        showConfirmButton: false,
        timer: 1500,
      });
      setSubmitting(false);

      console.log(error.response.data);
      console.log(error.response.status);
      console.log(error.response.headers);
    }
  }
};
