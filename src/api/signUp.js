import axios from "axios";
import Swal from "sweetalert2";

export const handleSignUp = async (values, { setSubmitting }, { navigate }) => {
  console.log("🚀 ~ file: login.js ~ line 2 ~ handleSubmit ~ values", values);
  const config = {
    headers: {
      "Content-Type": "application/json",
    },
  };

  const body = {
    role_id: 1,
    email: values.email,
    password: values.password,
    firstName: values.firstName,
    lastName: values.lastName,
  };
  try {
    const res = await axios.post(
      `https://my-clinic-apii.herokuapp.com/register`,
      body,
      config
    );
    console.log("🚀 ~ file: signUp.js ~ line 25 ~ res", res.data);
    await setSubmitting(false);
    await Swal.fire({
      position: "top-end",
      icon: "success",
      title: "Sign Up successful !!",
      showConfirmButton: false,
      timer: 1500,
    });
    await navigate("/signin");
  } catch (error) {
    if (error.response) {
      Swal.fire({
        position: "top-end",
        icon: "error",
        title: "Oops!! Can not sign up",
        showConfirmButton: false,
        timer: 1500,
      });
      setSubmitting(false);

      console.log(error.response.data);
      console.log(error.response.status);
      console.log(error.response.headers);
    }
  }
};
