import {
  postFriendData,
  postFriendId,
  resetFriendId,
} from "features/userSlice";

import Swal from "sweetalert2";
import axios from "axios";

// khi nhấn vào nút add friend, user 1 sẽ gửi request lên với tham số truyền vào là id của user 2 và body của user 1
// sau khi gửi request cho user 2, user 2 sẽ lấy từ API cái request đó ra và sẽ gửi request ngược lại cho user 1 1 biến boolean
// nếu như user 2 nhấn vào accept ( tức là gửi tham số isAccepted = true ) thì sẽ bắt đầu lấy id của user 1 để gửi request lên và lấy thông tin rồi đẩy thông tin đó vào list friend của user 2
// nếu như user 2 nhấn vào decline thì nó sẽ delete request đó ở trên json

// user 1 gửi request cho user 2
// nếu user 2 accept
// thì sẽ lấy thông tin của user 1 > đẩy thông tin của user 1 vào mảng Friends của user 2
// đồng thời cũng sẽ lấy thông tin của user đc gửi request đó ( user 2 ) > đẩy thông tin đó vào mảng Friends của user gửi request ( user 1 )
// nếu user đc gửi request ( user 2 ) nhấn decline thì sẽ lấy id của request đó và xóa khỏi mảng Friends Request của user đc gửi request

export const handleSearchFriends = async (
  e,
  keyword,
  { setUsersList },
  { setText },
  { setIsLoading },
  { dispatch }
) => {
  e.preventDefault();
  dispatch(resetFriendId());
  setIsLoading(true);
  const account = JSON.parse(localStorage.getItem("account"));

  try {
    const res = await axios.get(
      `https://my-clinic-apii.herokuapp.com/api/users/users?firstName=${keyword}`
    );

    await setUsersList(res.data);

    const res1 = await axios.get(
      `https://my-clinic-apii.herokuapp.com/api/users/users/${account.user.id}/friends`
    );

    const res2 = await axios.get(
      `https://my-clinic-apii.herokuapp.com/api/users/users/${account.user.id}/friendRequest`
    );

    const usersSearchList = await res.data;
    const userFriends = await res1.data;
    const userList2 = [];
    var userListId = null;

    const checkSameFriend = usersSearchList.filter((o1) =>
      userFriends.some((o2) => o1.id === o2.myId)
    );
    console.log(
      "🚀 ~ file: friends.js ~ line 57 ~ checkSameFriend",
      checkSameFriend
    );

    const checkFriendRequest = res2.data.filter((o1) =>
      usersSearchList.some((o2) => o1.myId === o2.id)
    );
    console.log(
      "🚀 ~ file: friends.js ~ line 60 ~ checkFriendRequest",
      checkFriendRequest
    );

    usersSearchList.forEach((el) => {
      console.log("🚀 ~ file: friends.js ~ line 66 ~ el", el.id);
      userList2.push(el.id);
    });
    console.log("🚀 ~ file: friends.js ~ line 69 ~ userList2", userList2);

    // userList2.forEach((el) => {
    //   userListId = el;
    //   // console.log(
    //   //   "🚀 ~ file: friends.js ~ line 75 ~ userList2.forEach ~ userListId",
    //   //   userListId
    //   // );
    //   getFriendRequest(userListId, { dispatch });
    // });

    if (checkSameFriend.length === 0) setText("Add friend");
    if (checkSameFriend.length !== 0) setText("See anamnesis");

    await setIsLoading(false);
  } catch (error) {
    setIsLoading(false);

    console.log(error.response.data);
    console.log(error.response.status);
    console.log(error.response.headers);
  }
};

export const handleSendingFriendRequest = async (
  userId,
  { setIsSending },
  { setIsSent },
  { dispatch }
) => {
  setIsSending(true);
  const account = JSON.parse(localStorage.getItem("account"));
  const userInfo = JSON.parse(localStorage.getItem("userInfo"));

  const config = {
    headers: {
      "Content-Type": "application/json",
    },
  };
  const body = {
    myId: account.user.id,
    firstName: account.user.firstName,
    lastName: account.user.lastName,
    avt: userInfo && userInfo.avt ? userInfo.avt : account.user.avt,
    email: account.user.email,
    userId: userId,
  };

  const res1 = await axios.get(
    `https://my-clinic-apii.herokuapp.com/api/users/users/${userId}/friendRequest`
  );

  const friendList = res1.data;

  const checkSameRequest = friendList.find((o1) => o1.myId === account.user.id);

  if (checkSameRequest) {
    setIsSent(true);
    setIsSending(false);
    Swal.fire({
      icon: "error",
      title: "Oops...",
      text: "You have sent request to this account already 🤧",
      showConfirmButton: false,
      timer: 2000,
    });
  } else {
    try {
      const res = await axios.post(
        `https://my-clinic-apii.herokuapp.com/api/users/users/${userId}/friendRequest`,
        body,
        config
      );

      console.log(
        "🚀 ~ file: friends.js ~ line 28 ~ đẩy request addfr lên cho bạn",
        res.data
      );

      if (res.data) {
        await setIsSent(true);
        await setIsSending(false);
        await Swal.fire({
          icon: "success",
          title: "Sent request successful ❤️",
          showConfirmButton: false,
          timer: 2000,
        });

        await dispatch(resetFriendId());
      }
    } catch (error) {
      if (error && error.response) {
        setIsSent(false);
        setIsSending(false);
        Swal.fire({
          icon: "error",
          title: "Oops...",
          text: "Can not send request 😢",
          showConfirmButton: false,
          timer: 2000,
        });

        console.log(error.response.data);
        console.log(error.response.status);
        console.log(error.response.headers);
      }
    }
  }
};

export const getRequest = async ({ setRequestList }, { setIsLoading }) => {
  const account = JSON.parse(localStorage.getItem("account"));

  try {
    const res = await axios.get(
      `https://my-clinic-apii.herokuapp.com/api/users/users/${account.user.id}/friendRequest`
    );
    console.log("🚀 ~ file: friends.js ~ line 105 ~ getRequest ~ res", res);

    setRequestList(res.data);
    setIsLoading(false);
  } catch (error) {
    console.log(error.response.data);
    console.log(error.response.status);
    console.log(error.response.headers);
  }
};

const getFriendRequest = async (friendId, { dispatch }) => {
  try {
    const res = await axios.get(
      `https://my-clinic-apii.herokuapp.com/api/users/users/${friendId}/friendRequest`
    );
    console.log(
      "🚀 ~ file: friends.js ~ line 105 ~ getRequest ~ res",
      res.data
    );

    res.data.forEach((el) => {
      console.log(
        "🚀 ~ file: friends.js ~ line 226 ~ res.data.forEach ~ el",
        el
      );

      dispatch(postFriendId(el));
    });

    // setTimeout(() => {
    // }, 5000);
  } catch (error) {
    console.log(error.response.data);
    console.log(error.response.status);
    console.log(error.response.headers);
  }
};

export const getFriendList = async (
  userId,
  { setFriendsList },
  { setIsLoading }
) => {
  setIsLoading(true);
  try {
    const res = await axios.get(
      `https://my-clinic-apii.herokuapp.com/api/users/users/${userId}/friends`
    );
    console.log("🚀 ~ file: friends.js ~ line 127 ~ getFriendList", res.data);

    setFriendsList(res.data);
    setIsLoading(false);
  } catch (error) {
    setIsLoading(false);
    console.log(error.response.data);
    console.log(error.response.status);
    console.log(error.response.headers);
  }
};

export const handleAddFriend = async (
  isAccepted,
  userId1,
  requestId,
  { setIsSending },
  { navigate }
) => {
  const account = JSON.parse(localStorage.getItem("account"));
  setIsSending(true);

  const config = {
    headers: {
      "Content-Type": "application/json",
    },
  };
  try {
    if (isAccepted === true) {
      const res = await axios.get(
        `https://my-clinic-apii.herokuapp.com/api/users/users/${userId1}`
      );
      console.log("🚀 ~ file: info.js ~ line 12 ~ res", res.data);

      if (res.data) {
        const res1 = await axios.post(
          `https://my-clinic-apii.herokuapp.com/api/users/users/${account.user.id}/friends`,
          {
            myId: res.data.id,
            age: res.data.age,
            anamnesis: res.data.anamnesis,
            avt: res.data.avt,
            email: res.data.email,
            firstName: res.data.firstName,
            height: res.data.height,
            lastName: res.data.lastName,
            phone: res.data.phone,
            weight: res.data.weight,
          },
          config
        );
        console.log(
          "🚀 ~ file: friends.js ~ line 148 ~ post friend cho mình",
          res1.data
        );

        const res2 = await axios.post(
          `https://my-clinic-apii.herokuapp.com/api/users/users/${userId1}/friends`,
          {
            myId: account.user.id,
            age: account.user.age,
            anamnesis: account.user.anamnesis,
            avt: account.user.avt,
            email: account.user.email,
            firstName: account.user.firstName,
            height: account.user.height,
            lastName: account.user.lastName,
            phone: account.user.phone,
            weight: account.user.weight,
          },
          config
        );
        console.log(
          "🚀 ~ file: friends.js ~ line 166 ~ handleAddFriend ~ post friend cho user1",
          res2.data
        );

        Swal.fire({
          icon: "success",
          title: "Two of you are now friends ❤️",
          showConfirmButton: false,
          timer: 2000,
        });

        const res3 = await axios.delete(
          `https://my-clinic-apii.herokuapp.com/api/users/friendRequest/${requestId}`
        );

        console.log(
          "🚀 ~ file: friends.js ~ line 146 ~ handleAddFriend ~ xóa lời mời",
          res3.data
        );

        setIsSending(false);
        navigate(0);
      }
    }
  } catch (error) {
    if (error && error.response) {
      Swal.fire({
        icon: "error",
        title: "Oops...",
        text: "Something went wrong!",
        showConfirmButton: false,
        timer: 2000,
      });
      console.log(error.response.data);
      console.log(error.response.status);
      console.log(error.response.headers);
    }
  }
};

export const handleDeclineFriend = async (
  isAccepted,
  requestId,
  { navigate }
) => {
  try {
    if (isAccepted === false) {
      const res = await axios.delete(
        `https://my-clinic-apii.herokuapp.com/api/users/friendRequest?id=${requestId}`
      );
      console.log("🚀 ~ file: friends.js ~ line 29 ~ getUserDetail ~ res", res);
      Swal.fire({
        icon: "success",
        title: "Gud bye 🤧",
        showConfirmButton: false,
        timer: 1500,
      });

      navigate(0);
    }
  } catch (error) {
    if (error && error.response) {
      Swal.fire({
        icon: "error",
        title: "Oops...",
        text: "Something went wrong!",
        showConfirmButton: false,
        timer: 1500,
      });
      console.log(error.response.data);
      console.log(error.response.status);
      console.log(error.response.headers);
    }
  }
};

export const getFriendDetail = async (friendId, { dispatch }) => {
  const account = JSON.parse(localStorage.getItem("account"));

  try {
    const res = await axios.get(
      `https://my-clinic-apii.herokuapp.com/api/users/users/${account.user.id}/friends?myId=${friendId}`
    );
    console.log(
      "🚀 ~ file: friends.js ~ line 222 ~ getFriendDetail ~ res",
      res
    );

    dispatch(postFriendData(res.data[0]));
  } catch (error) {
    console.log(error.response.data);
    console.log(error.response.status);
    console.log(error.response.headers);
  }
};
