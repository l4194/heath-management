import Swal from "sweetalert2";
import axios from "axios";

export const getSchedules = async (
  patientId,
  dateId,
  { setScheduleList },
  { setDateList },
  { setIsLoading }
) => {
  setIsLoading(true);
  try {
    const res1 = await axios.get(
      `https://my-clinic-apii.herokuapp.com/api/users/users/${patientId}/myScheduleDate`
    );
    console.log("🚀 ~ file: schedules.js ~ line 17 ~ res1", res1.data);

    setDateList(res1.data);

    if (dateId) {
      const res = await axios.get(
        `https://my-clinic-apii.herokuapp.com/api/users/myScheduleDate/${dateId}/mySchedule`
      );
      setScheduleList(res.data);
    } else {
      const res = await axios.get(
        `https://my-clinic-apii.herokuapp.com/api/users/users/${patientId}/mySchedule`
      );
      setScheduleList(res.data);
    }

    setIsLoading(false);
  } catch (error) {
    if (error && error.response) {
      Swal.fire({
        icon: "error",
        title: "Oops...",
        text: "Something went wrong 🤧",
        showConfirmButton: false,
        timer: 2000,
      });
      console.log(error.response.data);
      console.log(error.response.status);
      console.log(error.response.headers);
    }
  }
};

export const getOldReport = async (patientId, { setReportDetail }) => {
  try {
    const res = await axios.get(
      `https://my-clinic-apii.herokuapp.com/api/users/users/${patientId}/oldReport`
    );
    console.log(
      "🚀 ~ file: schedules.js ~ line 54 ~ getOldReport ~ res",
      res.data
    );

    setReportDetail(res.data[0]);
  } catch (error) {
    if (error && error.response) {
      Swal.fire({
        icon: "error",
        title: "Oops...",
        text: "Something went wrong 🤧",
        showConfirmButton: false,
        timer: 2000,
      });
      console.log(error.response.data);
      console.log(error.response.status);
      console.log(error.response.headers);
    }
  }
};

export const getUserDetail = async (patientId, { setUserDetail }) => {
  try {
    const res = await axios.get(
      `https://my-clinic-apii.herokuapp.com/api/users/users/${patientId}`
    );
    console.log("🚀 ~ file: info.js ~ line 12 ~ res", res.data);
    setUserDetail(res.data);
  } catch (error) {
    console.log(error.response.data);
    console.log(error.response.status);
    console.log(error.response.headers);
  }
};
