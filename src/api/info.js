import { getDownloadURL, ref, uploadBytesResumable } from "firebase/storage";

import Swal from "sweetalert2";
import axios from "axios";
import { storage } from "../firebase/firebase";
import { userData } from "features/userSlice";

export const uploadFiles = (
  patientId,
  imgUrl,
  userDetail,
  values,
  { setSubmitting },
  { setImgUrl }
) => {
  console.log("🚀 ~ file: info.js ~ line 20 ~ values", values);
  const userInfo = JSON.parse(localStorage.getItem("userInfo"));
  const account = JSON.parse(localStorage.getItem("account"));

  if (account && account.user.avt && typeof values.image !== "object") {
    console.log("account");
    handleUpdateProfile(patientId, userDetail, account.user.avt, values, {
      setSubmitting,
    });
  } else if (userInfo && userInfo.avt && typeof values.image !== "object") {
    console.log("userInfo");
    handleUpdateProfile(patientId, userDetail, userInfo.avt, values, {
      setSubmitting,
    });
  } else if (values.image && values.image !== "") {
    console.log("up lên firebase");
    const storageRef = ref(storage, `files/${values.image.name}`);
    const uploadTask = uploadBytesResumable(storageRef, values.image);

    uploadTask.on(
      "state_changed",
      (snapshot) => {
        Math.round((snapshot.bytesTransferred / snapshot.totalBytes) * 100);
        // setProgress(prog);
      },
      (error) => console.log(error),
      () => {
        getDownloadURL(uploadTask.snapshot.ref).then((url) => {
          console.log("File available at", url);
          imgUrl = url;
          setImgUrl(imgUrl);

          if (imgUrl)
            handleUpdateProfile(patientId, userDetail, imgUrl, values, {
              setSubmitting,
            });
        });
      }
    );
  }
};

export const getUserDetail = async (
  userDetail,
  patientId,
  { setUserDetail },
  { dispatch }
) => {
  try {
    const res = await axios.get(
      `https://my-clinic-apii.herokuapp.com/api/users/users/${patientId}`
    );
    console.log("🚀 ~ file: info.js ~ line 12 ~ res", res.data);

    userDetail = res.data;
    setUserDetail(userDetail);

    dispatch(userData(userDetail));
  } catch (error) {
    console.log(error.response.data);
    console.log(error.response.status);
    console.log(error.response.headers);
  }
};

const handleUpdateProfile = async (
  patientId,
  userDetail,
  imgUrl,
  values,
  { setSubmitting }
) => {
  const userInfo = JSON.parse(localStorage.getItem("userInfo"));

  const config = {
    headers: {
      "Content-Type": "application/json",
    },
  };

  const body = {
    role_id: 1,
    firstName: values.firstName,
    lastName: values.lastName,
    id_card: values.id_card,
    avt: imgUrl,
    age: values.age,
    address: values.address,
    phone: values.phone,
    email: values.email,
    weight: values.weight,
    height: values.height,
    anamnesis: values.anamnesis,
    userId: patientId,
    password:
      userDetail && userDetail.password
        ? userDetail.password
        : userInfo.password,
  };
  console.log("🚀 ~ file: info.js ~ line 112 ~ body", body);
  try {
    const res1 = await axios.get(
      `https://my-clinic-apii.herokuapp.com/api/users/users/${patientId}/oldReport`
    );
    console.log("🚀 ~ file: info.js ~ line 111 ~ res1", res1.data);
    const index = res1.data.findIndex((el) => el.userId === patientId);
    console.log("🚀 ~ file: info.js ~ line 117 ~ index", index);

    if (index === 0) {
      console.log("update report cũ");
      const res = await axios.get(
        `https://my-clinic-apii.herokuapp.com/api/users/users/${patientId}`
      );

      if (res.data) {
        const res3 = await axios.put(
          `https://my-clinic-apii.herokuapp.com/api/users/oldReport/${res1.data[index].id}`,
          res.data
        );
        console.log("🚀 ~ file: info.js ~ line 143 ~ res1", res3.data);

        const res2 = await axios.put(
          `https://my-clinic-apii.herokuapp.com/api/users/users/${patientId}`,
          body
        );
        console.log(
          "🚀 ~ file: info.js ~ line 18 ~ handleUpdateProfile ~ res",
          res2.data
        );
        localStorage.setItem("userInfo", JSON.stringify(res2.data));
      }
    }
    if (index === -1) {
      console.log("push mới report cũ");
      const res = await axios.put(
        `https://my-clinic-apii.herokuapp.com/api/users/users/${patientId}`,
        body
      );

      const res2 = await axios.post(
        `https://my-clinic-apii.herokuapp.com/api/users/users/${patientId}/oldReport`,
        body,
        config
      );

      console.log(
        "🚀 ~ file: info.js ~ line 18 ~ handleUpdateProfile ~ res",
        res.data
      );
      console.log("🚀 ~ file: info.js ~ line 130 ~ res1", res2.data);
      localStorage.setItem("userInfo", JSON.stringify(res.data));
    }

    await setSubmitting(false);

    await Swal.fire({
      position: "top-end",
      icon: "success",
      title: "Updated your profile successful❤️",
      showConfirmButton: false,
      timer: 1500,
    });
  } catch (error) {
    if (error && error.response) {
      Swal.fire({
        position: "top-end",
        icon: "error",
        title: "Oops!! Can not update your profile",
        showConfirmButton: false,
        timer: 1500,
      });

      console.log(error.response.data);
      console.log(error.response.status);
      console.log(error.response.headers);
    }
  }
};
