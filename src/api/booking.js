import {
  doctorDetail,
  doctorList,
  postDoctorDate,
} from "features/doctorsSlice";

import Swal from "sweetalert2";
import axios from "axios";

export const getDoctorsList = async (faculty, { dispatch }) => {
  try {
    const res = await axios.get(
      `https://my-clinic-apii.herokuapp.com/api/doctors/doctors?faculty=${faculty}`
    );
    // console.log("🚀 ~ file: booking.js ~ line 8 ~ getDoctors ~ res", res.data);

    dispatch(doctorList(res.data));
  } catch (error) {
    console.log(error.response.data);
    console.log(error.response.status);
    console.log(error.response.headers);
  }
};

export const getDoctorDetail = async (doctorId, { dispatch }) => {
  // console.log("🚀 ~ file: booking.js ~ line 28 ~ doctorId", doctorId);
  try {
    const res = await axios.get(
      `https://my-clinic-apii.herokuapp.com/api/doctors/doctors/${doctorId}`
    );
    // console.log("🚀 ~ file: booking.js ~ line 32 ~ res", res.data);
    dispatch(doctorDetail(res.data));
  } catch (error) {
    console.log(error.response.data);
    console.log(error.response.status);
    console.log(error.response.headers);
  }
};

export const handleBooking = async (doctorId, values, { setSubmitting }) => {
  console.log(
    "🚀 ~ file: booking.js ~ line 46 ~ handleBooking ~ values",
    values
  );
  const account = JSON.parse(localStorage.getItem("account"));
  const userInfo = JSON.parse(localStorage.getItem("userInfo"));

  const myAccountId = userInfo && userInfo.id ? userInfo.id : account.user.id;

  const name =
    userInfo && userInfo.firstName && userInfo.lastName
      ? `${userInfo.firstName} ${userInfo.lastName} `
      : account.user.firstName && account.user.lastName
      ? `${account.user.firstName} ${account.user.lastName}`
      : values.name;

  const email =
    userInfo && userInfo.email
      ? userInfo.email
      : account.user.email
      ? account.user.email
      : values.email;

  const anamnesis =
    userInfo && userInfo.anamnesis
      ? userInfo.anamnesis
      : account.user.anamnesis
      ? account.user.anamnesis
      : values.anamnesis;

  const phone =
    userInfo && userInfo.phone
      ? userInfo.phone
      : account.user.phone
      ? account.user.phone
      : values.phone;

  const avt = userInfo && userInfo.avt ? userInfo.avt : account.user.avt;
  const age = userInfo && userInfo.age ? userInfo.age : account.user.age;

  const config = {
    headers: {
      "Content-Type": "application/json",
    },
  };
  const body = {
    userId: myAccountId,
    name: name,
    avt: avt,
    email: email,
    age: age,
    phone: phone,
    anamnesis: anamnesis,

    service: values.service,
    time: values.time,
    date: values.date,

    doctorId: values.doctorId,
  };
  console.log("🚀 ~ file: booking.js ~ line 106 ~ handleBooking ~ body", body);
  try {
    const res = await axios.post(
      `https://my-clinic-apii.herokuapp.com/api/doctors/doctors/${doctorId}/booking_list`,
      body,
      config
    );

    console.log(
      "🚀 ~ file: booking.js ~ line 39 ~ handleBooking ~ res",
      res.data
    );

    const res1 = await axios.get(
      `https://my-clinic-apii.herokuapp.com/api/doctors/doctors/${doctorId}`
    );

    if (res1.data) {
      const res2 = await axios.post(
        `https://my-clinic-apii.herokuapp.com/api/users/users/${account.user.id}/mySchedule`,
        {
          name: name,
          avt: avt,
          email: email,

          anamnesis: anamnesis,
          service: values.service,
          time: values.time,
          date: values.date,

          doctor: `${res1.data.firstName} ${res1.data.lastName}`,
          doctorAvt: res1.data.avt,
          doctorId: values.doctorId,
          appointmentId: res.data.id,
          userId: Number(myAccountId),
        },
        config
      );
    }
    const res3 = await axios.get(
      `https://my-clinic-apii.herokuapp.com/api/users/myScheduleDate`
    );

    const res4 = await axios.get(
      `https://my-clinic-apii.herokuapp.com/api/users/mySchedule`
    );

    const checkSameDate = res3.data.filter((o1) => o1.date === values.date);
    const checkSameDate2 = res4.data.filter((o1) =>
      checkSameDate.some((o2) => o1.date === o2.date)
    );

    if (checkSameDate.length !== 0) {
      checkSameDate.forEach((el) => {
        const result = checkSameDate2.filter((o1) => o1.date === el.date);

        result.forEach((el1) => {
          axios.patch(
            `https://my-clinic-apii.herokuapp.com/api/users/mySchedule/${el1.id}`,
            {
              myScheduleDateId: el.id,
            }
          );
        });
        axios.patch(
          `https://my-clinic-apii.herokuapp.com/api/users/myScheduleDate/${el.id}`,
          {
            date: el.date,
          }
        );
      });
    } else {
      const res5 = await axios.post(
        `https://my-clinic-apii.herokuapp.com/api/users/myScheduleDate`,
        {
          userId: myAccountId,
          date: values.date,
        },
        config
      );

      const result1 = await res4.data.filter(
        (o1) => o1.date === res5.data.date
      );

      if (result1.length !== 0) {
        result1.forEach((el) => {
          axios.patch(
            `https://my-clinic-apii.herokuapp.com/api/users/mySchedule/${el.id}`,
            {
              myScheduleDateId: res5.data.id,
            }
          );
        });
      }
    }

    await setSubmitting(false);
    await Swal.fire({
      icon: "success",
      title: "Book successful ❤️",
      showConfirmButton: false,
      timer: 1500,
    });

    window.location.href = `yourSchedule/${account.user.id}`;
  } catch (error) {
    if (error && error.response) {
      Swal.fire({
        icon: "error",
        title: "Oops...",
        text: "You have sent request to this account already 🤧",
        showConfirmButton: false,
        timer: 2000,
      });
      console.log(error.response.data);
      console.log(error.response.status);
      console.log(error.response.headers);
    }
  }
};

export const getBookingList = async (doctorId, date, { dispatch }) => {
  console.log("🚀 ~ file: booking.js ~ line 153 ~ getBookingList ~ date", date);
  try {
    const res = await axios.get(
      `https://my-clinic-apii.herokuapp.com/api/doctors/doctors/${doctorId}/booking_list`
    );
    console.log(
      "🚀 ~ file: booking.js ~ line 120 ~ getBookingList ~ res",
      res.data
    );

    const res1 = await axios.get(
      `https://my-clinic-apii.herokuapp.com/api/doctors/doctors/${doctorId}`
    );
    console.log(
      "🚀 ~ file: booking.js ~ line 131 ~ getBookingList ~ res1",
      res1.data
    );

    const bookingList = await res.data;
    const dateBooking = date.toLocaleDateString();

    const result1 = bookingList.filter((el) => el.date === dateBooking);
    console.log(
      "🚀 ~ file: booking.js ~ line 187 ~ getBookingList ~ result1",
      result1
    );

    dispatch(postDoctorDate(result1));
  } catch (error) {
    console.log(error.response.data);
    console.log(error.response.status);
    console.log(error.response.headers);
  }
};

export const handleCancelBooking = async (id, appointmentId, { navigate }) => {
  try {
    Swal.fire({
      title: "Are you sure?",
      text: "You won't be able to revert this!",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Yes, cancel it!",
    }).then((result) => {
      if (result.isConfirmed) {
        axios.delete(
          `https://my-clinic-apii.herokuapp.com/api/users/mySchedule/${id}`
        );
        axios.delete(
          `https://my-clinic-apii.herokuapp.com/api/doctors/${appointmentId}`
        );

        Swal.fire({
          title: "Cancelled!",
          text: "Your appointment has been cancelled!!",
          icon: "success",
          showConfirmButton: false,
          timer: 1500,
        });

        setTimeout(() => {
          navigate(0);
        }, 2000);
      }
    });
  } catch (error) {
    console.log(error.response.data);
    console.log(error.response.status);
    console.log(error.response.headers);
  }
};
