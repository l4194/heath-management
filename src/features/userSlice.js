import { createSlice } from "@reduxjs/toolkit";

export const userSlice = createSlice({
  name: "user",
  initialState: {
    data: null,
    patientId: null,
    friendData: null,
    friendId: [],
  },
  reducers: {
    userData: (state, action) => {
      console.log(
        "🚀 ~ file: userSlice.js ~ line 11 ~ action.payload",
        action.payload
      );
      state.data = action.payload;
    },
    postPatientId: (state, action) => {
      console.log(
        "🚀 ~ file: userSlice.js ~ line 20 ~ action.payload",
        action.payload
      );
      state.patientId = action.payload;
    },

    postFriendData: (state, action) => {
      console.log(
        "🚀 ~ file: userSlice.js ~ line 29 ~ action.payload",
        action.payload
      );
      state.friendData = action.payload;
    },

    postFriendId: (state, action) => {
      console.log(
        "🚀 ~ file: userSlice.js ~ line 38 ~ state.friendId",
        action.payload
      );
      state.friendId.push(action.payload);
    },

    resetFriendId: (state, action) => {
      state.friendId = [];
    },
  },
});

export const {
  userData,
  postPatientId,
  postFriendData,
  postFriendId,
  resetFriendId,
} = userSlice.actions;

export default userSlice.reducer;
