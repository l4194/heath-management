import "./HealthReport.css";

import { Form, Formik } from "formik";
import React, { useEffect, useState } from "react";
import { getUserDetail, uploadFiles } from "api/info";
import { initialValues, validationSchema } from "formik/info";
import { useNavigate, useParams } from "react-router";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import HealthReportLeft from "features/components/HealthReportlLeft";
import HealthReportRight from "features/components/HealthReportRight";
import { faCircleArrowLeft } from "@fortawesome/free-solid-svg-icons";
import { useDispatch } from "react-redux";

HealthReport.propTypes = {};

function HealthReport(props) {
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const { patientId } = useParams();

  var [userDetail, setUserDetail] = useState(null);
  const [imgUrl, setImgUrl] = useState(null);

  useEffect(() => {
    getUserDetail(userDetail, patientId, { setUserDetail }, { dispatch });
  }, [patientId]);

  return (
    <Formik
      enableReinitialize={true}
      validationSchema={validationSchema}
      initialValues={initialValues}
      onSubmit={(values, { setSubmitting }) => {
        uploadFiles(
          patientId,
          imgUrl,
          userDetail,
          values,
          { setSubmitting },
          { setImgUrl }
        );
      }}
    >
      {(formikProps) => {
        const { isSubmitting, setFieldValue, values } = formikProps;
        console.log(
          "🚀 ~ file: index.jsx ~ line 45 ~ HealthReport ~ values",
          values
        );
        return (
          <Form className="health_report">
            <FontAwesomeIcon
              icon={faCircleArrowLeft}
              size="3x"
              className="health_report_back_icon"
              onClick={() => navigate("/")}
            />
            <div className="health_report_left">
              <HealthReportLeft
                userDetail={userDetail}
                isSubmitting={isSubmitting}
                setFieldValue={setFieldValue}
              />
            </div>
            <div className="health_report_right">
              <HealthReportRight userDetail={userDetail} />
            </div>
          </Form>
        );
      }}
    </Formik>
  );
}

export default HealthReport;
