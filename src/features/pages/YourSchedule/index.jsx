import "./YourSchedule.css";

import { useNavigate, useParams } from "react-router";

import Box from "@mui/material/Box";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import PropTypes from "prop-types";
import React from "react";
import Tab from "@mui/material/Tab";
import Tabs from "@mui/material/Tabs";
import Typography from "@mui/material/Typography";
import UpdateReportTag from "features/components/UpdateReportTag";
import YourScheduleTag from "components/YourScheduleTag";
import { faCircleArrowLeft } from "@fortawesome/free-solid-svg-icons";
import { getSchedules } from "api/schedules";

YourSchedule.propTypes = {};

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box sx={{ p: 3 }}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.number.isRequired,
  value: PropTypes.number.isRequired,
};

function a11yProps(index) {
  return {
    id: `simple-tab-${index}`,
    "aria-controls": `simple-tabpanel-${index}`,
  };
}

function YourSchedule(props) {
  const navigate = useNavigate();
  const { patientId } = useParams();

  const [value, setValue] = React.useState(0);
  const [scheduleList, setScheduleList] = React.useState([]);
  const [dateList, setDateList] = React.useState([]);
  const [isLoading, setIsLoading] = React.useState(false);
  const [dateId, setDateId] = React.useState(null);

  React.useEffect(() => {
    getSchedules(
      patientId,
      dateId,
      { setScheduleList },
      { setDateList },
      { setIsLoading }
    );
  }, [patientId, dateId]);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  return (
    <div className="yourSchedule">
      <FontAwesomeIcon
        icon={faCircleArrowLeft}
        size="3x"
        className="yourSchedule_back_icon"
        onClick={() => navigate(-1)}
      />
      <Box sx={{ width: "100%" }}>
        <Box sx={{ borderBottom: 1, borderColor: "divider" }}>
          <Tabs
            value={value}
            onChange={handleChange}
            aria-label="basic tabs example"
          >
            <Tab label="Your Schedule" {...a11yProps(0)} />
            <Tab label="Update report history" {...a11yProps(1)} />
          </Tabs>
        </Box>
        <TabPanel value={value} index={0}>
          <div className="yourSchedule_single">
            {scheduleList.length === 0 ? (
              <></>
            ) : (
              <div className="date section1" id="group">
                <h2>Date</h2>
                {dateList.map((item, key) => {
                  return (
                    <div key={key} className="remember1">
                      <label>
                        {item.date}
                        <input
                          type="radio"
                          name="radio group"
                          // defaultChecked={checked}
                          onChange={() => setDateId(item.id)}
                        />
                        <span className="checkmark1"></span>
                      </label>
                    </div>
                  );
                })}
              </div>
            )}
            <div className="yourSchedule_single_tag">
              <YourScheduleTag
                isLoading={isLoading}
                scheduleList={scheduleList}
                patientId={patientId}
              />
            </div>
          </div>
        </TabPanel>
        <TabPanel value={value} index={1}>
          <UpdateReportTag patientId={patientId} />
          {/* <UpdateReportTag /> */}
        </TabPanel>
      </Box>
    </div>
  );
}

export default YourSchedule;
