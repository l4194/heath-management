import React from "react";
import { useNavigate } from "react-router";
import "./home.css";

function HomePage() {
  const navigate = useNavigate();
  const account = JSON.parse(localStorage.getItem("account"));
  return (
    <div className="home">
      <div className="container">
        <div className="container_top">
          <button
            className="home_option"
            style={{ backgroundColor: "#EF476F" }}
            onClick={() => navigate(`/healthReport/${account.user.id}`)}
          >
            <p>Health Report</p>
          </button>
          <button
            className="home_option"
            style={{ backgroundColor: "#FFD166" }}
            onClick={() => navigate("/healthCheck")}
          >
            <p>Schedule Health Check</p>
          </button>
        </div>
        <div className="container_bot">
          <button
            className="home_option"
            style={{ backgroundColor: "#06D6A0" }}
            onClick={() => navigate("/friend")}
          >
            <p>Friends</p>
          </button>
          <button
            className="home_option"
            style={{ backgroundColor: "#3498db" }}
            onClick={() => navigate(`/yourSchedule/${account.user.id}`)}
          >
            <p>Your Health Check Schedule</p>
          </button>
        </div>
      </div>
    </div>
  );
}

export default HomePage;
