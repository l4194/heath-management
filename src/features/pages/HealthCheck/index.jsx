import { faCircleArrowLeft } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { CircularProgress } from "@mui/material";
import { getDoctorDetail, getDoctorsList, handleBooking } from "api/booking";
import SelectDoctor from "features/components/SelectDoctor";
import SelectService from "features/components/SelectService";
import SelectTime from "features/components/SelectTime";
import YourInfo from "features/components/YoutInfo";
import { Form, Formik } from "formik";
import { initialValues } from "formik/booking";
import React, { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import { useNavigate } from "react-router";
import "./HealthCheck.css";

HealthCheck.propTypes = {};

function HealthCheck(props) {
  const navigate = useNavigate();
  const dispatch = useDispatch();

  const [facultyValues, setFacultyValues] = useState("");
  const [doctorId, setDoctorId] = useState(null);

  useEffect(() => {
    getDoctorsList(facultyValues, { dispatch });
  }, [facultyValues]);

  useEffect(() => {
    getDoctorDetail(doctorId, { dispatch });
  }, [doctorId]);

  return (
    <Formik
      // validationSchema={createTourValidationSchema}
      initialValues={initialValues}
      onSubmit={(values, { setSubmitting }) => {
        handleBooking(doctorId, values, { setSubmitting });
      }}
    >
      {(formikProps) => {
        const { isSubmitting, setFieldValue, values } = formikProps;
        console.log(
          "🚀 ~ file: index.jsx ~ line 43 ~ HealthCheck ~ values",
          values
        );

        return (
          <Form className="health_check">
            <FontAwesomeIcon
              icon={faCircleArrowLeft}
              size="3x"
              className="health_check_back_icon"
              onClick={() => navigate(-1)}
            />
            <div className="health_check_info">
              <span>Your Information</span>
              <div className="health_check_info_content">
                <YourInfo />
              </div>
            </div>
            <div className="health_check_service">
              <span>Select Service</span>
              <div className="health_check_service_content">
                <SelectService setFacultyValues={setFacultyValues} />
              </div>
            </div>
            <div className="health_check_doctor">
              <span>Select Doctor</span>
              <div className="health_check_doctor_content">
                <SelectDoctor setDoctorId={setDoctorId} />
              </div>
            </div>
            <div className="health_check_time">
              <span>Select Time</span>
              <div className="health_check_time_content">
                <SelectTime doctorId={doctorId} setFieldValue={setFieldValue} />
              </div>
            </div>
            <span className="health_check_warning">
              !!! Double-check before clicking the accept !!!
            </span>
            <div className="health_check_delacc">
              <button
                className="health_check_delacc_btn"
                style={{ backgroundColor: "#FF7675" }}
                onClick={() => navigate("/")}
              >
                Cancel
              </button>
              <button
                type="submit"
                className="health_check_delacc_btn"
                style={{ backgroundColor: "#55EFC4" }}
              >
                {isSubmitting ? (
                  <CircularProgress color="success" size={21} />
                ) : (
                  "Accept"
                )}
              </button>
            </div>
          </Form>
        );
      }}
    </Formik>
  );
}

export default HealthCheck;
