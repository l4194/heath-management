import "./Friend.css";

import {
  Navigate,
  Route,
  Routes,
  useLocation,
  useNavigate,
} from "react-router";
import React, { Suspense, useEffect, useState } from "react";
import { faCircleArrowLeft, faSearch } from "@fortawesome/free-solid-svg-icons";
import { getRequest, handleSearchFriends } from "api/friends";

import AddFriendTag from "features/components/AddFriendTag";
import Badge from "@mui/material/Badge";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import FriendReqTag from "features/components/FriendReqTag";
import { Loading } from "components/Loading";
import YourFriendTag from "features/components/YourFriendTag";
import { useDispatch } from "react-redux";

Friend.propTypes = {};

function Friend(props) {
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const location = useLocation();

  const [change, setChange] = useState(0);
  const [usersList, setUsersList] = useState([]);
  const [keyword, setKeyword] = useState(null);
  const [text, setText] = useState("");
  const [requestList, setRequestList] = useState([]);
  const [isLoading, setIsLoading] = useState(false);

  useEffect(() => {
    getRequest({ setRequestList }, { setIsLoading });
  }, []);

  const changeColor = (id, route) => {
    setChange(id);
    navigate(`${route}`);
  };

  return (
    <div className="friend">
      <div className="friend_header">
        <FontAwesomeIcon
          onClick={() => navigate("/")}
          icon={faCircleArrowLeft}
          size="3x"
          className="friend_header_back_icon"
        />
        <button
          onClick={() => changeColor(1, "addfriend")}
          className={
            change === 1 || location.pathname === `/friend/addfriend`
              ? "friend_header_tags"
              : "friend_header_tags_unclick"
          }
        >
          Find Friends
        </button>
        <button
          onClick={() => changeColor(2, "yourfriend")}
          className={
            change === 2 || location.pathname === `/friend/yourfriend`
              ? "friend_header_tags"
              : "friend_header_tags_unclick"
          }
        >
          Your Friends
        </button>
        <button
          onClick={() => changeColor(3, "friendrequest")}
          className={
            change === 3 || location.pathname === `/friend/friendrequest`
              ? "friend_header_tags"
              : "friend_header_tags_unclick"
          }
        >
          <Badge
            badgeContent={
              requestList && requestList.length > 0 ? requestList.length : 0
            }
            color="primary"
          >
            Friends Resquest
          </Badge>
        </button>
      </div>
      <div className="friend_body">
        <div className="friend_body_layer">
          <div className="friend_body_search">
            <form
              onSubmit={(e) =>
                handleSearchFriends(
                  e,
                  keyword,
                  { setUsersList },
                  { setText },
                  { setIsLoading },
                  { dispatch }
                )
              }
              className="friend_body_searchbar"
            >
              <button type="submit" className="friend_body_searchbtn">
                <FontAwesomeIcon icon={faSearch} />
                Search
              </button>
              <input
                type="text"
                onChange={(e) => setKeyword(e.target.value)}
                className="friend_body_searchinput"
              />
            </form>
          </div>
          <div className="friend_body_main">
            <Routes>
              <Route path="/" element={<Navigate to="addfriend" />} />
              <Route
                index
                path="addfriend"
                element={
                  <Suspense fallback={<Loading />}>
                    <AddFriendTag
                      isLoading={isLoading}
                      usersList={usersList}
                      text={text}
                    />
                  </Suspense>
                }
              />
              <Route
                path="yourfriend"
                element={
                  <Suspense fallback={<Loading />}>
                    <YourFriendTag />
                  </Suspense>
                }
              />
              <Route
                path="friendrequest"
                element={
                  <Suspense fallback={<Loading />}>
                    <FriendReqTag requestList={requestList} />
                  </Suspense>
                }
              />
            </Routes>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Friend;
