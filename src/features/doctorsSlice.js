import { createSlice } from "@reduxjs/toolkit";

export const doctorSlice = createSlice({
  name: "doctor",
  initialState: {
    doctorList: [],
    doctorDetail: null,
    doctorTime: [],
    doctorDate: [],
  },
  reducers: {
    doctorList: (state, action) => {
      console.log(
        "🚀 ~ file: doctorSlice.js ~ line 11 ~ action.payload",
        action.payload
      );
      state.doctorList = action.payload;
    },
    doctorDetail: (state, action) => {
      console.log(
        "🚀 ~ file: doctorsSlice.js ~ line 20 ~ action.payload",
        action.payload
      );
      state.doctorDetail = action.payload;
    },
    postDoctorTime: (state, action) => {
      console.log(
        "🚀 ~ file: doctorsSlice.js ~ line 28 ~ action.payload",
        action.payload
      );
      state.doctorTime.push(action.payload);
    },
    postDoctorDate: (state, action) => {
      state.doctorDate = action.payload;
    },
  },
});

export const { doctorList, doctorDetail, postDoctorTime, postDoctorDate } =
  doctorSlice.actions;

export default doctorSlice.reducer;
