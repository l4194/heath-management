import "./SelectDoctor.css";

import { Field } from "formik";
import { Images } from "constants/images";
import React from "react";
import { useSelector } from "react-redux";

SelectDoctor.propTypes = {};

function SelectDoctor(props) {
  const { setDoctorId } = props;
  const doctorList = useSelector((state) => state.doctor.doctorList);

  const handleSetDoctorId = (id) => {
    setDoctorId(id);
  };

  return (
    <div className="selectDoctor">
      {doctorList && doctorList.length === 0 ? (
        <div
          style={{
            width: "1810px",
            height: "265px",
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
            flexDirection: "column",
          }}
        >
          <img src={Images.EMPTY} alt="" />
          Please select service to see available doctors
        </div>
      ) : (
        doctorList.map((item, key) => {
          return (
            <div key={key} className="selectDoctor_single">
              <label
                onClick={() => handleSetDoctorId(item.id)}
                className="selectDoctor_single_label"
              >
                <div className="selectDoctor_single_label_info">
                  <img
                    src={item.avt ? item.avt : Images.DEFAULT_USER}
                    className="selectDoctor_single_label_info_img"
                    alt=""
                  />
                  <div className="selectDoctor_single_label_info_name">
                    <span className="selectDoctor_single_label_info_name_full">
                      Dr.{item.firstName} {item.lastName}{" "}
                    </span>
                    <span className="selectDoctor_single_label_info_name_email">
                      {item.email}
                    </span>
                  </div>
                </div>
                <Field type="radio" name="doctorId" value={`${item.id}`} />
                <span className="checkmark"></span>
              </label>
            </div>
          );
        })
      )}
    </div>
  );
}

export default SelectDoctor;
