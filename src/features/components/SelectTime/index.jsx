import { getBookingList } from "api/booking";
import { Field } from "formik";
import moment from "moment";
import React, { useEffect, useState } from "react";
import { Calendar } from "react-calendar";
import "react-calendar/dist/Calendar.css";
import { useDispatch } from "react-redux";
import { useSelector } from "react-redux";
import Swal from "sweetalert2";
import "./SelectTime.css";

SelectTime.propTypes = {};

function SelectTime(props) {
  const { doctorId, setFieldValue } = props;

  const dispatch = useDispatch();

  const doctorDetail = useSelector((state) => state.doctor.doctorDetail);
  const doctorDate = useSelector((state) => state.doctor.doctorDate);

  var [date, setDate] = useState(new Date());

  useEffect(() => {
    getBookingList(doctorId, date, { dispatch });
  }, [doctorId, date]);

  const handleCheckBookedDateTime = (time) => {
    // const currentTime = moment(date.getTime()).format("LT");
    // // currentTime.slice(-8, -7);
    // console.log(
    //   "🚀 ~ file: index.jsx ~ line 28 ~ handleCheckBookedDateTime ~ currentTime",
    //   currentTime.slice(0, 2) + "h"
    // );
    const checkSameTime = doctorDate.find((o1) => o1.time === time);

    if (checkSameTime) return true;
    return false;
  };

  const handleOnChange = (newValue) => {
    const currentDate = moment(new Date()).format("L");
    const dateValue = moment(newValue).format("L");

    if (dateValue < currentDate) {
      Swal.fire({
        icon: "error",
        title: "Oops...",
        text: "You couldn't make an appointment on this date",
        showConfirmButton: false,
        timer: 2000,
      });
    } else {
      date = newValue;
      setDate(date);
      setFieldValue("date", date.toLocaleDateString());
    }
  };

  let day = date.getDay();
  switch (day) {
    case 0:
      day = "Sunday";
      break;
    case 1:
      day = "Monday";
      break;
    case 2:
      day = "Tuesday";
      break;
    case 3:
      day = "Wednesday";
      break;
    case 4:
      day = "Thurday";
      break;
    case 5:
      day = "Friday";
      break;
    case 6:
      day = "Saturday";
      break;
    default:
      break;
  }
  return (
    <div className="selectTime">
      <div className="selectTime_calendarpicker">
        <div className="calendar-container">
          <Calendar
            onChange={(newValue) => {
              handleOnChange(newValue);
            }}
            value={date}
          />
        </div>
      </div>
      <div className="selectTime_timepicker">
        {!doctorDetail ? (
          <div>Please select doctor to see available time for booking</div>
        ) : (
          doctorDetail.working_time.map((item, key) => {
            return (
              <div key={key} className="selectTime_timepicker_single">
                <label className="container1">
                  <Field type="radio" name="time" value={`${item.time}`} />
                  <span
                    // className="checkmark1"
                    className={
                      handleCheckBookedDateTime(item.time)
                        ? "checkmark_disable"
                        : "checkmark1"
                    }
                  >
                    {`${item.time}`}
                  </span>
                </label>
              </div>
            );
          })
        )}
      </div>
    </div>
  );
}

export default SelectTime;
