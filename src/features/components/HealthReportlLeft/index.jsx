import { faCameraRetro } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { CircularProgress } from "@mui/material";
import { Images } from "constants/images";
import { ErrorMessage } from "formik";
import React, { useState } from "react";
import { useNavigate } from "react-router";
import "./HealthReportLeft.css";

function HealthReportLeft(props) {
  const { userDetail, isSubmitting, setFieldValue } = props;

  const [image, setImage] = useState(null);

  const navigate = useNavigate();

  const onImageChange = (e) => {
    if (e.target.files && e.target.files[0]) {
      setImage(URL.createObjectURL(e.target.files[0]));
      setFieldValue("image", e.target.files[0]);
    }
  };
  return (
    <div className="healthReportLeft">
      <div className="healthReportLeft_border">
        {image ? (
          <img src={image} alt="" className="healthReportLeft_border_img" />
        ) : (
          <img
            src={
              userDetail && userDetail.avt
                ? userDetail.avt
                : Images.DEFAULT_USER
            }
            className="healthReportLeft_border_img"
            alt="user_img"
          />
        )}
        <label className="healthReportLeft_border_camera">
          <FontAwesomeIcon
            icon={faCameraRetro}
            size="2x"
            className="healthReportLeft_border_camera_icon"
          />
          <input name="image" onChange={onImageChange} type="file" />
        </label>
        <ErrorMessage name="image" />
      </div>
      <div className="healthReportLeft_btn">
        <button
          disabled={isSubmitting}
          type="button"
          className="healthReportLeft_btn_cancel"
          style={{ backgroundColor: "#FF7675" }}
          onClick={() => navigate("/")}
        >
          Cancel
        </button>
        <button
          disabled={isSubmitting}
          type="submit"
          className="healthReportLeft_btn_apply"
          style={{ backgroundColor: "#55EFC4" }}
        >
          {isSubmitting ? (
            <CircularProgress color="success" size={21} />
          ) : (
            "Apply"
          )}
        </button>
      </div>
    </div>
  );
}

export default HealthReportLeft;
