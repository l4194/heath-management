import React from "react";
import "./SelectService.css";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCircleExclamation } from "@fortawesome/free-solid-svg-icons";
import { Field } from "formik";

SelectService.propTypes = {};

function SelectService(props) {
  const { setFacultyValues } = props;

  const handleSetFaculty = (faculty) => {
    setFacultyValues(faculty);
  };

  return (
    <div className="SelectService">
      <div className="SelectService_single">
        <label
          className="SelectService_single_label"
          onClick={() => handleSetFaculty("Neuropathy")}
        >
          <span>Neuropathy</span>
          <Field type="radio" name="service" value="Neuropathy" />
          <label></label>
          <span className="checkmark"></span>
          <FontAwesomeIcon
            icon={faCircleExclamation}
            size="xs"
            className="SelectService_single_icon"
          />
        </label>
      </div>
      <div className="SelectService_single">
        <label
          className="SelectService_single_label"
          onClick={() => handleSetFaculty("Ears, Nose And Throat")}
        >
          <span>Ears, Nose And Throat</span>
          <Field type="radio" name="service" value="Ears, Nose And Throat" />
          <span className="checkmark"></span>
          <FontAwesomeIcon
            icon={faCircleExclamation}
            size="xs"
            className="SelectService_single_icon"
          />
        </label>
      </div>
      <div className="SelectService_single">
        <label
          className="SelectService_single_label"
          onClick={() => handleSetFaculty("Stomach")}
        >
          <span className="title">Stomach</span>
          <Field type="radio" name="service" value="Stomach" />
          <span className="checkmark"></span>
          <FontAwesomeIcon
            icon={faCircleExclamation}
            size="xs"
            className="SelectService_single_icon"
          />
        </label>
      </div>
      <div className="SelectService_single">
        <label
          onClick={() => handleSetFaculty("Skin Disorders")}
          className="SelectService_single_label"
        >
          {" "}
          <span>Skin Disorders</span>
          <Field type="radio" name="service" value="Skin Disorders" />
          <span className="checkmark"></span>
          <FontAwesomeIcon
            icon={faCircleExclamation}
            size="xs"
            className="SelectService_single_icon"
          />
        </label>
      </div>
    </div>
  );
}

export default SelectService;
