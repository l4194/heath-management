import "./AddFriendTag.css";

import React, { useState } from "react";
import { faUserCircle, faUserPlus } from "@fortawesome/free-solid-svg-icons";
import { getFriendDetail, handleSendingFriendRequest } from "api/friends";
import { useDispatch, useSelector } from "react-redux";

import { CircularProgress } from "@mui/material";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import HealthStatus from "../HealthStatus";
import { Images } from "constants/images";

AddFriendTag.propTypes = {};

function AddFriendTag(props) {
  const { isLoading, usersList, text } = props;

  const account = JSON.parse(localStorage.getItem("account"));
  const friendRequestList = useSelector((state) => state.user.friendId);
  console.log(
    "🚀 ~ file: index.jsx ~ line 20 ~ AddFriendTag ~ friendRequestList",
    friendRequestList
  );

  const dispatch = useDispatch();

  const [isOpened, setIsOpened] = useState(false);
  const [isSending, setIsSending] = useState(false);
  const [isSent, setIsSent] = useState(false);

  const checkIsSentRequest = (id) => {
    console.log("🚀 ~ file: index.jsx ~ line 28 ~ checkIsSentRequest ~ id", id);
    if (friendRequestList.length !== 0) {
      const result = friendRequestList.filter(
        (o1) => o1.myId === account.user.id
      );
      console.log(
        "🚀 ~ file: index.jsx ~ line 30 ~ checkIsSentRequest ~ result",
        result
      );
      if (result.myId === id) return true;
      else return false;
    }
  };

  const handleGetFriendDetail = (friendId) => {
    getFriendDetail(friendId, { dispatch });
    setIsOpened(!isOpened);
  };

  const handleSendingRequest = (userId) => {
    handleSendingFriendRequest(
      userId,
      { setIsSending },
      { setIsSent },
      { dispatch }
    );
  };

  return (
    <>
      {usersList &&
        usersList.length === 0 &&
        (isLoading ? (
          <div></div>
        ) : (
          <div className="AddFriendTag_empty">
            <img src={Images.EMPTY} className="AddFriendTag_empty_img" alt="" />
            <p className="AddFriendTag_empty_text">
              {" "}
              Please search for find some more awesome friends
            </p>
          </div>
        ))}

      {isLoading ? (
        <div className="loadingio-spinner-ripple-ym1gnjjxd6d">
          <div className="ldio-qcfj2i4nqi">
            <div></div>
            <div></div>
          </div>
        </div>
      ) : (
        usersList.map((item, key) => {
          if (item.id === account.user.id) {
            return (
              <div className="AddFriendTag_empty">
                <img
                  src={Images.EMPTY}
                  className="AddFriendTag_empty_img"
                  alt=""
                />
                <p className="AddFriendTag_empty_text">
                  {" "}
                  Can not find your friends
                </p>
              </div>
            );
          }
          return (
            <div key={key} className="AddFriendTag">
              <div className="AddFriendTag_ava">
                <div className="AddFriendTag_ava_border">
                  {item.avt ? (
                    <img
                      src={item.avt}
                      alt={`${item.firstName} ${item.lastName}`}
                      className="AddFriendTag_ava_icon"
                    />
                  ) : (
                    <FontAwesomeIcon
                      icon={faUserCircle}
                      className="AddFriendTag_ava_icon"
                    />
                  )}
                </div>
              </div>
              <div className="AddFriendTag_noti">
                <span className="AddFriendTag_noti_name">
                  {`${item.firstName} ${item.lastName}`}{" "}
                </span>
                <span className="AddFriendTag_noti_email">{item.email} </span>
                <div
                  // onClick={() => handleSendingFriendRequest(item.id)}
                  className="AddFriendTag_noti_btn"
                >
                  <button
                    onClick={
                      text === "See anamnesis"
                        ? () => handleGetFriendDetail(item.id)
                        : () => handleSendingRequest(item.id)
                    }
                    disabled={isSent}
                    // className={
                    //   checkIsSentRequest || isSent
                    //     ? "AddFriendTag_noti_btn_disable"
                    //     : "AddFriendTag_noti_btn_able"
                    // }
                    className={
                      isSent
                        ? "AddFriendTag_noti_btn_disable"
                        : "AddFriendTag_noti_btn_able"
                    }
                  >
                    <FontAwesomeIcon
                      icon={
                        text === "See anamnesis" ? faUserCircle : faUserPlus
                      }
                      className="AddFriendTag_noti_btn_icon"
                    />
                    {isSending ? (
                      <CircularProgress color="success" size={21} />
                    ) : (
                      `${text}`
                    )}
                  </button>
                </div>
              </div>
            </div>
          );
        })
      )}

      <HealthStatus isOpened={isOpened} setIsOpened={setIsOpened} />
    </>
  );
}

export default AddFriendTag;
