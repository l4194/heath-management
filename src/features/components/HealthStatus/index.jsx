import { faXmarkCircle } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Images } from "constants/images";
import React from "react";
import { useSelector } from "react-redux";
import "./HealthStatus.css";

function HealthStatus(props) {
  const { isOpened, setIsOpened } = props;
  const friendData = useSelector((state) => state.user.friendData);
  return (
    <div className={isOpened ? "healthStatus" : "disable_healthStatus"}>
      <div className="healthStatus_form">
        <div className="healthStatus_form_header">
          <div className="healthStatus_form_header_bg"></div>
          <img
            className="healthStatus_form_header_bg_img"
            src={
              friendData && friendData.avt
                ? friendData.avt
                : Images.DEFAULT_USER
            }
            alt=""
          />
        </div>
        <div className="healthStatus_form_body">
          <div className="healthStatus_form_body_header">
            Health Status
            <FontAwesomeIcon
              onClick={() => setIsOpened(!isOpened)}
              icon={faXmarkCircle}
              size="1x"
              className="healthStatus_form_body_header_exit"
            />
          </div>
          <div className="healthStatus_form_body_body">
            <div className="healthStatus_form_body_line">
              <label className="healthStatus_form_body_line_label">
                Full Name:
              </label>
              <span className="healthStatus_form_body_line_content">
                {friendData && friendData.firstName
                  ? friendData.firstName
                  : "Can not find your name"}{" "}
                {friendData && friendData.lastName ? friendData.lastName : ""}
              </span>
            </div>
            <div className="healthStatus_form_body_line">
              <label className="healthStatus_form_body_line_label">Age:</label>
              <span className="healthStatus_form_body_line_content">
                {friendData && friendData.age
                  ? friendData.age
                  : "Can not find your age"}{" "}
              </span>
            </div>
            <div className="healthStatus_form_body_line">
              <label className="healthStatus_form_body_line_label">
                Weight:
              </label>
              <span className="healthStatus_form_body_line_content">
                {friendData && friendData.weight
                  ? friendData.weight
                  : "Can not find your weight"}{" "}
                <span>Kg</span>
              </span>
            </div>
            <div className="healthStatus_form_body_line">
              <label className="healthStatus_form_body_line_label">
                Height:
              </label>
              <span className="healthStatus_form_body_line_content">
                {friendData && friendData.height
                  ? friendData.height
                  : "Can not find your height"}{" "}
                <span>Cm</span>
              </span>
            </div>
            <div className="healthStatus_form_body_line_2">
              <label className="healthStatus_form_body_line_label_2">
                Anamnesis:
              </label>
              <span className="healthStatus_form_body_line_content_2">
                {friendData && friendData.anamnesis
                  ? friendData.anamnesis
                  : "Can not find your anamnesis"}
              </span>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default HealthStatus;
