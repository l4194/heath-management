import { ErrorMessage, Field } from "formik";
import React from "react";
import "./HealthReportRight.css";

function HealthReportRight(props) {
  const { userDetail } = props;
  return (
    <div className="healthReportRight">
      <div className="healthReportRight_single">
        <label>First Name</label>
        <Field
          placeholder={userDetail ? userDetail.firstName : ""}
          type="text"
          name="firstName"
        />
        <ErrorMessage name="firstName" />
      </div>
      <div className="healthReportRight_single">
        <label>Last Name</label>
        <Field
          placeholder={userDetail ? userDetail.lastName : ""}
          type="text"
          name="lastName"
        />
        <ErrorMessage name="lastName" />
      </div>
      <div className="healthReportRight_single_short">
        <label>Age</label>
        <Field
          placeholder={userDetail ? userDetail.age : ""}
          type="text"
          name="age"
        />
        <ErrorMessage name="age" />
      </div>
      <div className="healthReportRight_single_short">
        <label>Card ID</label>
        <Field
          placeholder={userDetail ? userDetail.id_card : ""}
          type="text"
          name="id_card"
        />
        <ErrorMessage name="id_card" />
      </div>
      <div className="healthReportRight_single_short">
        <label>Phone</label>
        <Field
          placeholder={userDetail ? userDetail.phone : ""}
          type="text"
          name="phone"
        />
        <ErrorMessage name="phone" />
      </div>
      <div className="healthReportRight_single">
        <label>Address</label>
        <Field
          placeholder={userDetail ? userDetail.address : ""}
          type="text"
          name="address"
        />
        <ErrorMessage name="address" />
      </div>
      <div className="healthReportRight_single">
        <label>Email</label>
        <Field
          placeholder={userDetail ? userDetail.email : ""}
          type="email"
          name="email"
        />
        <ErrorMessage name="email" />
      </div>
      <div className="healthReportRight_single">
        <label>Weight (Kg)</label>
        <Field
          placeholder={userDetail ? userDetail.weight : ""}
          type="text"
          name="weight"
        />
        <ErrorMessage name="weight" />
      </div>
      <div className="healthReportRight_single">
        <label>Height (Cm)</label>
        <Field
          placeholder={userDetail ? userDetail.height : ""}
          type="text"
          name="height"
        />
        <ErrorMessage name="height" />
      </div>
      <div className="healthReportRight_single_long">
        <label>Anamnesis</label>
        <Field
          placeholder={userDetail ? userDetail.anamnesis : ""}
          as="textarea"
          name="anamnesis"
        />
        <ErrorMessage name="anamnesis" />
      </div>
    </div>
  );
}

export default HealthReportRight;
