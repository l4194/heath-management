import { faFileLines, faUserCircle } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React, { useState, useEffect } from "react";
import "./YourFriendTag.css";
import { getFriendDetail, getFriendList } from "api/friends";
import { Images } from "constants/images";
import HealthStatus from "../HealthStatus";
import { useDispatch } from "react-redux";

YourFriendTag.propTypes = {};

function YourFriendTag(props) {
  const account = JSON.parse(localStorage.getItem("account"));
  const dispatch = useDispatch();

  const [friendsList, setFriendsList] = useState([]);
  const [isLoading, setIsLoading] = useState(false);
  const [isOpened, setIsOpened] = useState(false);

  useEffect(() => {
    getFriendList(account.user.id, { setFriendsList }, { setIsLoading });
  }, [account.user.id]);

  const handleGetFriendDetail = (friendId) => {
    console.log(
      "🚀 ~ file: index.jsx ~ line 25 ~ handleGetFriendDetail ~ friendId",
      friendId
    );
    getFriendDetail(friendId, { dispatch });
    setIsOpened(!isOpened);
  };
  return (
    <>
      {isLoading ? (
        <div class="loadingio-spinner-ripple-ym1gnjjxd6d">
          <div class="ldio-qcfj2i4nqi">
            <div></div>
            <div></div>
          </div>
        </div>
      ) : friendsList && friendsList.length === 0 ? (
        <div className="YourFriendTag_empty">
          <p className="YourFriendTag_empty_text"> You dont have any friends</p>
          <img src={Images.EMPTY} className="YourFriendTag_empty_img" alt="" />
        </div>
      ) : (
        friendsList.map((item, key) => {
          return (
            <div key={key} className="YourFriendTag">
              <div className="YourFriendTag_ava">
                <div className="YourFriendTag_ava_border">
                  {account.user.avt ? (
                    <img
                      src={item.avt}
                      alt=""
                      className="YourFriendTag_ava_icon"
                    />
                  ) : (
                    <FontAwesomeIcon
                      icon={faUserCircle}
                      className="YourFriendTag_ava_icon"
                    />
                  )}
                </div>
              </div>
              <div className="YourFriendTag_noti">
                <span className="YourFriendTag_noti_name">
                  {item ? `${item.firstName} ${item.lastName}` : ""}{" "}
                </span>
                <span className="YourFriendTag_noti_email">
                  {item ? item.email : ""}
                </span>
                <div className="YourFriendTag_noti_btn">
                  <button onClick={() => handleGetFriendDetail(item.myId)}>
                    <FontAwesomeIcon
                      icon={faFileLines}
                      className="YourFriendTag_noti_btn_icon"
                    />{" "}
                    Health status
                  </button>
                </div>
              </div>
            </div>
          );
        })
      )}
      <HealthStatus isOpened={isOpened} setIsOpened={setIsOpened} />
    </>
  );
}

export default YourFriendTag;
