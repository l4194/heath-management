import { Field } from "formik";
import React from "react";
import "./YourInfo.css";

YourInfo.propTypes = {};

function YourInfo(props) {
  const userInfo = JSON.parse(localStorage.getItem("userInfo"));
  const account = JSON.parse(localStorage.getItem("account"));
  return (
    <div className="YourInfo">
      <div className="YourInfo_single">
        <label>Full Name</label>
        <Field
          name="name"
          placeholder={
            account
              ? `${account.user.firstName} ${account.user.lastName}`
              : `${userInfo.firstName} ${userInfo.lastName}`
          }
          autoFocus
          type="text"
        />
      </div>
      <div className="YourInfo_single">
        <label>Phone number</label>
        <Field
          name="phone"
          placeholder={
            account && account.user.phone
              ? `${account.user.phone} `
              : `${userInfo.phone}`
          }
          type="text"
        />
      </div>
      <div className="YourInfo_single">
        <label>Email</label>
        <Field
          name="email"
          placeholder={
            account && account.user.email
              ? `${account.user.email} `
              : `${userInfo.email}`
          }
          type="text"
        />
      </div>
      <div className="YourInfo_single_2">
        <label>Anamnesis</label>
        <Field
          placeholder={
            userInfo && userInfo.anamnesis
              ? `${userInfo.anamnesis}`
              : `${account.user.anamnesis} `
          }
          name="anamnesis"
          as="textarea"
        />
      </div>
    </div>
  );
}

export default YourInfo;
