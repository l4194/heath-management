import { faUserCircle } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { handleAddFriend, handleDeclineFriend } from "api/friends";
import { Images } from "constants/images";
import React, { useState } from "react";
import { useNavigate } from "react-router";
import "./FriendReqTag.css";

FriendReqTag.propTypes = {};

function FriendReqTag(props) {
  const { requestList } = props;

  const navigate = useNavigate();

  var [isAccepted, setIsAccepted] = useState(false);
  const [isSending, setIsSending] = useState(false);

  const handleAccept = (userId1, requestId) => {
    console.log("Accept");
    isAccepted = true;
    setIsAccepted(isAccepted);
    console.log(
      "🚀 ~ file: index.jsx ~ line 22 ~ handleAccept ~ isAccepted",
      isAccepted
    );
    handleAddFriend(
      isAccepted,
      userId1,
      requestId,
      { setIsSending },
      { navigate }
    );
  };

  const handleDecline = (requestId) => {
    console.log("Decline");
    isAccepted = false;
    setIsAccepted(isAccepted);
    console.log(
      "🚀 ~ file: index.jsx ~ line 22 ~ handleAccept ~ isAccepted",
      isAccepted
    );
    handleDeclineFriend(isAccepted, requestId, { setIsSending }, { navigate });
  };

  return (
    <>
      {requestList && requestList.length === 0 ? (
        <div className="FriendReqTag_empty">
          <p className="FriendReqTag_empty_text">
            {" "}
            Please search for find some more awesome friends
          </p>
          <img src={Images.EMPTY} className="FriendReqTag_empty_img" alt="" />
        </div>
      ) : (
        requestList.map((item, key) => {
          return (
            <div key={key} className="FriendReqTag">
              <div className="FriendReqTag_ava">
                <div className="FriendReqTag_ava_border">
                  {item.avt ? (
                    <img
                      src={item.avt}
                      alt={`${item.firstName} ${item.lastName}`}
                      className="FriendReqTag_ava_icon"
                    />
                  ) : (
                    <FontAwesomeIcon
                      icon={faUserCircle}
                      className="FriendReqTag_ava_icon"
                    />
                  )}
                </div>
              </div>
              <div className="FriendReqTag_noti">
                <span className="FriendReqTag_noti_name">{`${item.firstName} ${item.lastName}`}</span>
                <span className="FriendReqTag_noti_email">{item.email}</span>
                <div className="FriendReqTag_noti_btn">
                  <button
                    onClick={() => handleAccept(item.myId, item.id)}
                    style={{ backgroundColor: "#55EFC4" }}
                  >
                    Accept
                  </button>
                  <button
                    onClick={() => handleDecline(item.id)}
                    style={{ backgroundColor: "#FF7675" }}
                  >
                    Decline
                  </button>
                </div>
              </div>
            </div>
          );
        })
      )}
    </>
  );
}

export default FriendReqTag;
