import "./UpdateReportTag.css";

import React, { useEffect, useState } from "react";
import { getOldReport, getUserDetail } from "api/schedules";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Images } from "constants/images";
import Typography from "@mui/material/Typography";
import { faArrowRightLong } from "@fortawesome/free-solid-svg-icons";
import moment from "moment";

// import Accordion from "@mui/material/Accordion";
// import AccordionDetails from "@mui/material/AccordionDetails";
// import AccordionSummary from "@mui/material/AccordionSummary";
// import ExpandMoreIcon from "@mui/icons-material/ExpandMore";

UpdateReportTag.propTypes = {};

function UpdateReportTag(props) {
  const { patientId } = props;

  const [reportDetail, setReportDetail] = useState(null);
  const [userDetail, setUserDetail] = useState(null);

  useEffect(() => {
    getOldReport(patientId, { setReportDetail });
    getUserDetail(patientId, { setUserDetail });
  }, [patientId]);

  return (
    <div className="updateReportTag">
      {/* <Accordion className="updateReportTag_accor">
        <AccordionSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls="panel1a-content"
          id="panel1a-header"
          className="updateReportTag_date"
        >
          <Typography className="updateReportTag_date">11/12/2022</Typography>
        </AccordionSummary>
        <AccordionDetails>
        </AccordionDetails>
      </Accordion> */}
      <Typography className="updateReportTag_date">
        Updated at :{" "}
        {userDetail ? moment(userDetail.updatedAt).format("L") : ""} -{" "}
        {userDetail ? moment(userDetail.createdAt).fromNow() : ""}
      </Typography>
      <Typography>
        <div className="updateReportTag_single">
          <p>Image:</p>
          <div className="updateReportTag_single_before">
            <img
              alt=""
              src={
                reportDetail && reportDetail.avt
                  ? reportDetail.avt
                  : Images.ABOUT
              }
            />
          </div>
          <FontAwesomeIcon icon={faArrowRightLong} size="2x" />
          <div className="updateReportTag_single_after">
            <img
              alt=""
              src={
                userDetail && userDetail.avt
                  ? userDetail.avt
                  : Images.DEFAULT_USER
              }
            />
          </div>
        </div>
        <div className="updateReportTag_single">
          <p>Anamnesis:</p>
          <div className="updateReportTag_single_before">
            {reportDetail ? reportDetail.anamnesis : ""}
          </div>
          <FontAwesomeIcon icon={faArrowRightLong} size="2x" />
          <div className="updateReportTag_single_after">
            {userDetail ? userDetail.anamnesis : ""}
          </div>
        </div>
      </Typography>
    </div>
  );
}

export default UpdateReportTag;
