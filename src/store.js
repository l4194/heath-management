import { configureStore } from "@reduxjs/toolkit";
import userReducer from "features/userSlice";
import doctorReducer from "features/doctorsSlice";
const store = configureStore({
  reducer: {
    user: userReducer,
    doctor: doctorReducer,
  },
});

export default store;
