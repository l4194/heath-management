import "./YourScheduleTag.css";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Images } from "constants/images";
import React from "react";
import { faTrash } from "@fortawesome/free-solid-svg-icons";
import { handleCancelBooking } from "api/booking";
import { useNavigate } from "react-router";

YourScheduleTag.propTypes = {};

function YourScheduleTag(props) {
  const { isLoading, scheduleList } = props;
  const navigate = useNavigate();

  return (
    <>
      {isLoading ? (
        <></>
      ) : (
        scheduleList &&
        scheduleList.length === 0 && (
          <div className="empty_schedule">
            <img src={Images.EMPTY} alt="" />
            You have not created an appointment yet!!
          </div>
        )
      )}

      {isLoading ? (
        <div className="loadingio-spinner-ripple-ym1gnjjxd6d">
          <div className="ldio-qcfj2i4nqi">
            <div></div>
            <div></div>
          </div>
        </div>
      ) : (
        <div className="yourSchedule_item_container">
          {scheduleList &&
            scheduleList.length !== 0 &&
            scheduleList.map((item, key) => {
              return (
                <div key={key} className="yourScheduleTag">
                  <div className="yourScheduleTag_info">
                    <div className="yourScheduleTag_info_ava">
                      <img
                        className="yourScheduleTag_info_ava_img"
                        src={item.doctorAvt ? item.doctorAvt : Images.ABOUT}
                        alt=""
                      />
                    </div>
                    <div className="yourScheduleTag_info_name">
                      <span className="yourScheduleTag_info_name_full">
                        Dr.{item.doctor}
                      </span>
                      <span className="yourScheduleTag_info_name_user">
                        {item.doctor}
                      </span>
                    </div>
                  </div>
                  <span className="yourScheduleTag_service">
                    {item.service}
                  </span>
                  <span className="yourScheduleTag_date">{item.date} </span>
                  <span className="yourScheduleTag_time">{item.time} </span>
                  <FontAwesomeIcon
                    onClick={() =>
                      handleCancelBooking(
                        item.id,
                        item.doctorId,
                        item.appointmentId,
                        { navigate }
                      )
                    }
                    icon={faTrash}
                    size="lg"
                    className="yourScheduleTag_del"
                  />
                </div>
              );
            })}
        </div>
      )}
    </>
  );
}

export default YourScheduleTag;
