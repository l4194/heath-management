import React from "react";
import "./footer.css";

export const Footer = () => {
  if (window.location.pathname === "/signin") return <div></div>;
  if (window.location.pathname === "/signup") return <div></div>;
  return (
    <footer className="footer">
      <p>@Copyright by Tuan&Nghi</p>
    </footer>
  );
};
