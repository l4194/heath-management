import { Logout } from "@mui/icons-material";
import {
  Avatar,
  Divider,
  IconButton,
  ListItemIcon,
  Menu,
  MenuItem,
  Tooltip,
} from "@mui/material";
import { menuItem } from "constants/global";
import { Images } from "constants/images";
import React from "react";
import { useNavigate } from "react-router";
import "./header.css";

export const Header = () => {
  const account = JSON.parse(localStorage.getItem("account"));

  const navigate = useNavigate();
  const [anchorEl, setAnchorEl] = React.useState(null);

  const open = Boolean(anchorEl);

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const handleLogout = () => {
    localStorage.removeItem("userInfo");
    localStorage.removeItem("account");
    window.location.href = "/signin";
  };

  const handleRedirect = (name) => {
    console.log("🚀 ~ file: index.jsx ~ line 44 ~ handleRedirect ~ name", name);
    if (name === "Health Report") navigate(`/healthReport/${account.user.id}`);
    if (name === "Schedule Health Check")
      navigate(`/yourSchedule/${account.user.id}`);
    if (name === "Friends") navigate(`/friend`);
    if (name === "Your Information")
      navigate(`/healthReport/${account.user.id}`);
  };

  if (window.location.pathname === "/signin") return <div></div>;
  if (window.location.pathname === "/signup") return <div></div>;

  return (
    <div className="header">
      <div className="header_logo_border2">
        <img
          onClick={() => navigate("/")}
          className="header_logo_border_img2"
          src={Images.LOGO}
          alt="logo"
        />
      </div>
      {/* {(window.location.pathname === "/healthCheck" ||
        window.location.pathname === "/friend/" ||
        window.location.pathname === "/healthReport/" ||
        window.location.pathname === "/yourSchedule") && (
        <div className="header_option_border">
          <Link to="/home">Health Report</Link>
          <Link to="/home">Schedule Health Check</Link>
          <Link to="/home">Friends</Link>
          <Link to="/home">Your Information</Link>
        </div>
      )} */}

      <div className="header_user_border">
        <Tooltip title="Account settings">
          <IconButton
            onClick={handleClick}
            size="small"
            sx={{ ml: 2 }}
            aria-controls={open ? "account-menu" : undefined}
            aria-haspopup="true"
            aria-expanded={open ? "true" : undefined}
          >
            <Avatar sx={{ width: 32, height: 32 }}>M</Avatar>
          </IconButton>
        </Tooltip>
        <Menu
          anchorEl={anchorEl}
          id="account-menu"
          open={open}
          onClose={handleClose}
          onClick={handleClose}
          PaperProps={{
            elevation: 0,
            sx: {
              overflow: "visible",
              filter: "drop-shadow(0px 2px 8px rgba(0,0,0,0.32))",
              mt: 1.5,
              "& .MuiAvatar-root": {
                width: 32,
                height: 32,
                ml: -0.5,
                mr: 1,
              },
              "&:before": {
                content: '""',
                display: "block",
                position: "absolute",
                top: 0,
                right: 14,
                width: 10,
                height: 10,
                bgcolor: "background.paper",
                transform: "translateY(-50%) rotate(45deg)",
                zIndex: 0,
              },
            },
          }}
          transformOrigin={{ horizontal: "right", vertical: "top" }}
          anchorOrigin={{ horizontal: "right", vertical: "bottom" }}
        >
          <MenuItem>
            <Avatar /> My account
          </MenuItem>
          {(window.location.pathname === "/friend/addfriend" ||
            window.location.pathname === "/friend/yourfriend" ||
            window.location.pathname === "/friend/friendrequest" ||
            window.location.pathname === `/healthReport/${account.user.id}` ||
            window.location.pathname === "/healthCheck" ||
            window.location.pathname === "/yourSchedule") &&
            menuItem.map((item, key) => {
              return (
                <MenuItem key={key} onClick={() => handleRedirect(item.name)}>
                  <Avatar /> {item.name}
                </MenuItem>
              );
            })}

          <Divider />

          <MenuItem onClick={() => handleLogout()}>
            <ListItemIcon>
              <Logout fontSize="small" />
            </ListItemIcon>
            Logout
          </MenuItem>
        </Menu>
      </div>
    </div>
  );
};
