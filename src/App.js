import "assets/styles/GlobalStyles.css";
import { Header } from "components/Header";
import { Footer } from "components/Footer";
import { Loading } from "components/Loading";
import Error from "components/NotFound";

import React, { Suspense } from "react";
import { BrowserRouter, Navigate, Route, Routes } from "react-router-dom";

const HomePage = React.lazy(() => import("./features/pages/Home/index"));
const SignIn = React.lazy(() => import("./features/pages/SignIn/index"));
const SignUp = React.lazy(() => import("./features/pages/SignUp/index"));
const Friend = React.lazy(() => import("./features/pages/Friend/index"));
const HealthReport = React.lazy(() =>
  import("./features/pages/HealthReport/index")
);
const HealthCheck = React.lazy(() =>
  import("./features/pages/HealthCheck/index")
);
const YourSchedule = React.lazy(() => import("./features/pages/YourSchedule"));

function App() {
  const user = localStorage.getItem("account");
  return (
    <BrowserRouter>
      <Header />

      <Routes>
        {!user && (
          <>
            <Route path="/" element={<Navigate to={"signin"} />} />

            <Route
              index
              path="signin"
              element={
                <Suspense fallback={<Loading />}>
                  <SignIn />
                </Suspense>
              }
            />

            <Route
              index
              path="signup"
              element={
                <Suspense fallback={<Loading />}>
                  <SignUp />
                </Suspense>
              }
            />
          </>
        )}

        {user && (
          <>
            <Route path="/" element={<Navigate to="home" />} />

            <Route
              path="friend/*"
              element={
                <Suspense fallback={<Loading />}>
                  <Friend />
                </Suspense>
              }
            />

            <Route
              path="healthReport/:patientId"
              element={
                <Suspense fallback={<Loading />}>
                  <HealthReport />
                </Suspense>
              }
            />

            <Route
              path="healthCheck"
              element={
                <Suspense fallback={<Loading />}>
                  <HealthCheck />
                </Suspense>
              }
            />

            <Route
              path="yourSchedule/:patientId"
              element={
                <Suspense fallback={<Loading />}>
                  <YourSchedule />
                </Suspense>
              }
            />

            <Route
              index
              path="home"
              element={
                <Suspense fallback={<Loading />}>
                  <HomePage />
                </Suspense>
              }
            />
          </>
        )}
        <Route path="*" element={<Error />} />
      </Routes>
      <Footer />
    </BrowserRouter>
  );
}

export default App;
