import * as Yup from "yup";

export const initialValues = {
  email: "",
  password: "",
};

export const validationSchema = Yup.object({
  email: Yup.string()
    .email("Invalid email address")
    .required("Please enter your email!!"),
  password: Yup.string()
    .required("Please Enter your password")
    .min(1, "Password is Invalid!!"),
});
