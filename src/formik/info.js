import * as Yup from "yup";

const SUPPORTED_FORMATS = ["image/jpg", "image/jpeg", "image/gif", "image/png"];
const userInfo = JSON.parse(localStorage.getItem("userInfo"));
const account = JSON.parse(localStorage.getItem("account"));

export const initialValues = {
  id_card:
    userInfo && userInfo.id_card
      ? userInfo.id_card
      : account.user.id_card
      ? account.user.id_card
      : "",
  firstName:
    userInfo && userInfo.firstName
      ? userInfo.firstName
      : account.user.firstName
      ? account.user.firstName
      : "",
  lastName:
    userInfo && userInfo.lastName
      ? userInfo.lastName
      : account.user.lastName
      ? account.user.lastName
      : "",
  email:
    userInfo && userInfo.email
      ? userInfo.email
      : account.user.email
      ? account.user.email
      : "",
  phone:
    userInfo && userInfo.phone
      ? userInfo.phone
      : account.user.phone
      ? account.user.phone
      : "",
  age:
    userInfo && userInfo.age
      ? userInfo.age
      : account.user.age
      ? account.user.age
      : "",
  address:
    userInfo && userInfo.address
      ? userInfo.address
      : account.user.age
      ? account.user.age
      : "",
  weight:
    userInfo && userInfo.weight
      ? userInfo.weight
      : account.user.weight
      ? account.user.weight
      : "",
  height:
    userInfo && userInfo.height
      ? userInfo.height
      : account.user.height
      ? account.user.height
      : "",
  anamnesis:
    userInfo && userInfo.anamnesis
      ? userInfo.anamnesis
      : account.user.anamnesis
      ? account.user.anamnesis
      : "",
  image:
    userInfo && userInfo.avt
      ? userInfo.avt
      : account.user.avt
      ? account.user.avt
      : null,
};

export const validationSchema = Yup.object({
  id_card: Yup.string()
    .min(1, "Please type your real id card 😒")
    .required("Please let us know your id card 🤔"),

  firstName: Yup.string()
    .min(1, "Please type your real name 😒")
    .required("Please let us know your first name 🤔"),

  lastName: Yup.string()
    .min(1, "Please type your real name 😒")
    .required("Please let us know your last name 🤔"),

  email: Yup.string()
    .email("Invalid email !!")
    .required("Please let us know your email 🤔"),

  phone: Yup.string()
    .required("Please let us know your Phone number 🤔")
    .matches(
      /^((\\+[1-9]{1,4}[ \\-]*)|(\\([0-9]{2,3}\\)[ \\-]*)|([0-9]{2,4})[ \\-]*)*?[0-9]{3,4}?[ \\-]*[0-9]{3,4}?$/,
      "Your phone number is not available"
    ),

  age: Yup.string().required("Please let us know your age 🤔"),

  address: Yup.string()
    .min("1", "Pleas type your real address 😒")
    .required("Please let us know your address 🤔"),

  weight: Yup.string()
    .min(1, "Please type your real weight 😒")
    .required("Please let us know your weight 🤔"),

  height: Yup.string()
    .min(1, "Please type your real height 😒")
    .required("Please let us know your height 🤔"),

  anamnesis: Yup.string()
    .min(1, "Invalid 😒")
    .required("Please let us know your anamnesis 🤔"),

  image: Yup.mixed()
    .nullable()
    // .test(
    //   "FILE_SIZE",
    //   "File too large",
    //   (value) => !value || (value && value.size >= 3000000)
    // )
    // .test(
    //   "FILE_FORMAT",
    //   "Unsupported file type",
    //   (value) => !value || (value && SUPPORTED_FORMATS.includes(value?.type))
    // )
    .required("Please choose image for your profile"),
});
