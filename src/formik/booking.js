import * as Yup from "yup";

const SUPPORTED_FORMATS = ["image/jpg", "image/jpeg", "image/gif", "image/png"];
const userInfo = localStorage.getItem("userInfo");
const account = JSON.parse(localStorage.getItem("account"));

export const initialValues = {
  name:
    userInfo && userInfo.firstName && userInfo.lastName
      ? `${userInfo.firstName} ${userInfo.lastName} `
      : account.user.firstName && account.user.lastName
      ? `${account.user.firstName} ${account.user.lastName}`
      : "",

  phone:
    userInfo && userInfo.phone
      ? userInfo.phone
      : account.user.phone
      ? account.user.phone
      : "",

  email:
    userInfo && userInfo.email
      ? userInfo.email
      : account.user.email
      ? account.user.email
      : "",

  anamnesis:
    userInfo && userInfo.anamnesis
      ? userInfo.anamnesis
      : account.user.anamnesis
      ? account.user.anamnesis
      : "",

  service: "",
  doctorId: null,
  time: "",
  date: "",
};

// export const validationSchema = Yup.object({
//   firstName: Yup.string()
//     .min(1, "Please type your real name 😒")
//     .required("Please let us know your first name 🤔"),

//   lastName: Yup.string()
//     .min(1, "Please type your real name 😒")
//     .required("Please let us know your last name 🤔"),

//   email: Yup.string()
//     .email("Invalid email !!")
//     .required("Please let us know your email 🤔"),

//   phone: Yup.string()
//     .required("Please let us know your Phone number 🤔")
//     .matches(
//       /^((\\+[1-9]{1,4}[ \\-]*)|(\\([0-9]{2,3}\\)[ \\-]*)|([0-9]{2,4})[ \\-]*)*?[0-9]{3,4}?[ \\-]*[0-9]{3,4}?$/,
//       "Your phone number is not available"
//     ),

//   age: Yup.string().required("Please let us know your age 🤔"),

//   time: Yup.string().required("Please choose the time you want 🤔"),

//   weight: Yup.string()
//     .min(1, "Please type your real weight 😒")
//     .required("Please let us know your weight 🤔"),

//   height: Yup.string()
//     .min(1, "Please type your real height 😒")
//     .required("Please let us know your height 🤔"),

//   anamnesis: Yup.string()
//     .min(1, "Invalid 😒")
//     .required("Please let us know your anamnesis 🤔"),

//   image: Yup.mixed()
//     .nullable()
//     .test(
//       "FILE_SIZE",
//       "File too large",
//       (value) => !value || (value && value.size <= 2000000)
//     )
//     .test(
//       "FILE_FORMAT",
//       "Unsupported file type",
//       (value) => !value || (value && SUPPORTED_FORMATS.includes(value?.type))
//     )
//     .required("Please choose image for your profile"),
// });
