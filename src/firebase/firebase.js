import firebase from "firebase/compat/app";
import "firebase/compat/auth";
import "firebase/compat/firestore";
import { getStorage } from "firebase/storage";

const firebaseConfig = {
  apiKey: "AIzaSyAqfUkRmKr4wCKMzEZYZRXDwiax9CXQYng",
  authDomain: "pv-food.firebaseapp.com",
  projectId: "pv-food",
  storageBucket: "pv-food.appspot.com",
  messagingSenderId: "641135637611",
  appId: "1:641135637611:web:738db0a02087d2c7686d2e",
  measurementId: "G-1YWWVYZY5H",
};
const app = firebase.initializeApp(firebaseConfig);
export const storage = getStorage(app);
