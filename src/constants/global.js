export const menuItem = [
  {
    id: 1,
    name: "Health Report",
  },
  {
    id: 2,
    name: "Schedule Health Check",
  },
  {
    id: 3,
    name: "Friends",
  },
  {
    id: 4,
    name: "Your Information",
  },
];
