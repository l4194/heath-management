import about from "assets/images/about_us.jpeg";
import empty from "assets/images/cart-empty.png";
import default_user from "assets/images/default_user.png";
import logo from "assets/images/logo.png";

export const Images = {
  EMPTY: empty,
  ABOUT: about,
  LOGO: logo,
  DEFAULT_USER: default_user,
};
